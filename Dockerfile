FROM python:latest

WORKDIR /mnt/d/Dana

RUN apt-get update
RUN apt-get install default-jdk -y
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

COPY json_convert.py ./

CMD [ "python", "./json_convert.py"]


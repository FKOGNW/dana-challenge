from pyspark.sql import SparkSession
from pyspark.sql.functions import col
import logging


logging.info("Initiate SPARK")
spark = SparkSession.builder \
    .master("local[*]") \
    .appName("Dana") \
    .getOrCreate()

logging.info("Reading json file")
df_business = spark.read.json("dataset/yelp_academic_dataset_business.json")

logging.info("Start writing business")
df_business.drop("attributes", "hours").repartition(100).write.mode("overwrite")\
            .option('header','true').csv("result/yelp_dataset_business_csv/")

logging.info("Start writing attributes")   
df_business.select(col("attributes.*"), col("business_id"))\
            .repartition(100).write.mode("overwrite")\
            .option('header','true').csv("result/yelp_dataset_attributes_csv/")
            
logging.info("Start writing hours")   
df_business.select(col("hours.*"), col("business_id"))\
            .repartition(100).write.mode("overwrite")\
            .option('header','true').csv("result/yelp_dataset_hours_csv/")
            

logging.info("Reading json file and covert to csv")
df_checkin = spark.read.json("dataset/yelp_academic_dataset_checkin.json")\
            .repartition(100).write.mode("overwrite")\
            .option('header','true').csv("result/yelp_dataset_checkin_csv/")


logging.info("Reading json file and convert to csv")
df_review = spark.read.json("dataset/yelp_academic_dataset_review.json")\
            .repartition(100).write.mode("overwrite")\
            .option('header','true').csv("result/yelp_dataset_review_csv/")


logging.info("Reading json file and convert to csv")
df_tip = spark.read.json("dataset/yelp_academic_dataset_tip.json")\
        .repartition(100).write.mode("overwrite")\
        .option('header','true').csv("result/yelp_dataset_tip_csv/")


logging.info("Reading json file and convert to csv")
df_user = spark.read.json("dataset/yelp_academic_dataset_user.json")\
        .repartition(100).write.mode("overwrite")\
        .option('header','true').csv("result/yelp_dataset_user_csv/")